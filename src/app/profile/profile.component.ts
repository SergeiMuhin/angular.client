import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  form: FormGroup;
  public signupInvalid: boolean = false;
  private formSubmitAttempt: boolean = false;
  private returnUrl: string = '';

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.email],
      username: ['', Validators.required],
      role: ['', Validators.required],
      avatar: ['', Validators.required],
    });

}
  async onSubmit() {
    this.signupInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const email = this.form.get('email').value;
        const username = this.form.get('username').value;
        const role = this.form.get('role').value;
        const avatar = this.form.get('avatar').value;

        const user = {
          email: email,
          username: username,
          role: role,
          avatar: avatar,
        };

        await this.userService.signup(user).subscribe((user: any) => {
          console.log(user);
          this.router.navigateByUrl('/login');
          //alert('Hi, my name is ' + user.username + " and I'm a " + user.role);
        });
      } catch (err) {
        this.signupInvalid = true;
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }
}
