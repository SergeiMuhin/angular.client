import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {DataService} from "../app/services/data.service"
import { EventEmitterService } from './event-emitter.service'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'vvvsa-client-app';
  showMySidebar : boolean = false;

  constructor(private router: Router,
    private data: DataService,
    private eventEmitterService: EventEmitterService
    ) {}

  ngOnInit(): void {
    this.router.navigateByUrl('/login');
    if (this.eventEmitterService.subsVar==undefined) {    
      this.eventEmitterService.subsVar = this.eventEmitterService.    
      invokeFirstComponentFunction.subscribe((name:string) => {    
        this.firstFunction();    
    }); 
  }
 
  }
  firstFunction() {   
  this.showMySidebar = this.data.getShowSidebar();
  }
} 
