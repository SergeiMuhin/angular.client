import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class InterceptedHttp implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.url.indexOf('/assets/') > -1) {
      return next.handle(request);
    }

    const authToken = true;

    if (authToken) {
      request = request.clone({
        url: environment.API_ENDPOINT + request.url,

        setHeaders: {
          'Content-Type': 'application/json',
        },
      });
      console.log(request.url);
    } else if (authToken) {
      request = request.clone({
        url: environment.API_ENDPOINT + request.url,
        //withCredentials: true,
        setHeaders: {
          //Authorization: `Bearer ${authToken}`,
          'Content-Type': 'application/json',
        },
      });
      console.log(request.url);
    } else {
      request = request.clone({
        url: environment.API_ENDPOINT + request.url,
        setHeaders: {
          'Content-Type': 'application/json',
        },
      });
      console.log(request.url);
    }

    return next.handle(request).pipe(
      tap(
        // Succeeds when there is a response; ignore other events
        (event) => {},
        // Operation failed; error is an HttpErrorResponse
        (error) => {
          if (error instanceof HttpErrorResponse) {
            alert(error.message);
            //if (error.status === 401) {
            //  console.debug('Http status code 401');
            //go to login
            //}
          }
        }
      )
    );
  }
}
