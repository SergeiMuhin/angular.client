import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  authenticationState = new BehaviorSubject(false);
  constructor(private http: HttpClient) {}

  baseUrl: string = '/users/';

  public login(userSpace: string, userEmail: string): any {
    return this.http.get(this.baseUrl + 'login/' + userSpace + '/' + userEmail);
  }

  public upData(userSpace: string, userEmail: string , user: any): any {
    return this.http.post(this.baseUrl + userSpace + '/' + userEmail, user);
  }

  public signup(user: any): any {
    return this.http.post(this.baseUrl, user);
  }
}
