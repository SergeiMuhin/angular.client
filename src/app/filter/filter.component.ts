import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FilterService } from '../services/filter.service';
import { DataService } from "../services/data.service";
import { EventEmitterService } from '../event-emitter.service'; 


@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  selectedSport: string = '';
  form: FormGroup;
  public FormMissingInformation: boolean = false;
  private formSubmitAttempt: boolean = false;
  private returnUrl: string = '';
  message:string;
  userEmail:string = 's@gmail.com';
  itemsGet:item [];
  fieldname:string = '';
  private myArray:Array<string>;
  center: google.maps.LatLngLiteral = { lat: 31.5, lng: 36 };
  zoom = 8;
  markers: any[] = [];
  sportChoice: Sport[]=[{value:'Key-0',viewValue:'Any'}];
  sports: Sport[] = [];
  showMyContainer: boolean = false;
  tempArray: item[] =[];
  fieldSports:string = '';
  defined:string[] = [];
  markerOptionID:string;
  selectedAddSportID:string;


  

  constructor(private fb: FormBuilder, private fiterService: FilterService, private data: DataService) {}

  async ngOnInit() {
    this.form = this.fb.group({
      Latitude: ['', Validators.required],
      Longitude: ['', Validators.required],
      selector: [null, Validators.required],
      Distance: ['',Validators.required],
    });
    this.SportAdder();
  }
 AddSportClick()
 {
   alert(this.markerOptionID);

 
  
   alert(this.selectedAddSportID);
  this.fiterService
  .postAddSport(this.userEmail,this.markerOptionID,this.selectedAddSportID)
  .subscribe((SportType: any) => {
    console.log("Success Added Sport");
  });
 }
        SubmitStatusClick(){
          alert('Add Status Clicked');
        }


  SportAdder()
  {
    this.fiterService
    .getSports()
    .subscribe((SportType: any) => {
      let SportTypes: any[] =[];
      SportTypes = SportType;
      let tempindex:number = 1;
      for (var sportPush of SportTypes) {
        if (this.sportChoice.find((test) => test.viewValue === sportPush.name) === undefined){
        let Sport: Sport = {value: '', viewValue: ''};
        Sport.value = sportPush.itemId.id;
        Sport.viewValue = sportPush.name;
        tempindex++;
        this.sportChoice.push(Sport);
        }
      }  
    });
  }

  addMarker(event: google.maps.MouseEvent) {
    this.markers = [];
    let marker: any = new google.maps.Marker;
    marker.markerOptions = { draggable: false,clickable: true};
    marker.markerOptions.icon = "https://developers.google.com/maps/documentation/javascript/examples/full/images/library_maps.png";
    marker.markerOptions.title = "UserLocation";
    marker.markerPositions = event.latLng.toJSON();
    this.markers.push(marker);
    console.log(marker);
    this.message = JSON.stringify(event.latLng.toJSON(), null, 2);    
    this.myArray = this.message.split(/,| |}/);
    this.form.controls.Latitude.setValue(this.myArray[3]);
    this.form.controls.Longitude.setValue(this.myArray[7]);
    this.showMyContainer = false;
  }

 
  
  async clickedMarker(event: google.maps.MouseEvent,markerOpt:string){
    let markerOption:string = markerOpt;
    this.markerOptionID = markerOpt;
    if(markerOption != 'UserLocation')
    {
      this.showMyContainer = true;
      this.fieldname = markerOpt;
      await this.fiterService
      .getItem(markerOption)
      .subscribe((item: any) => {
        this.fieldname = item.name;
      });


      await this.fiterService
      .getAllfieldChildren(markerOption,this.userEmail)
      .subscribe((fieldsSportsAvailable: any) => {
        this.tempArray = fieldsSportsAvailable;
        this.fieldSports = '';


        for(var tArray of this.tempArray)
        {
        var str1 = tArray.name;
        this.defined.push(tArray.name);
        var str2 = this.fieldSports;
        this.fieldSports = str2+' , ' +str1;
        }  

        
        let tempindex:number = 0;
        let SportTypes: any[] =[];
        for ( var ableSport of this.sportChoice )
        {
          if(ableSport.viewValue != 'Any')
          {
            if((this.defined.find((test) => test === ableSport.viewValue) === undefined ))
            {
            let Sport: Sport = {value: '', viewValue: ''};
            Sport.value = ableSport.value;
            Sport.viewValue = ableSport.viewValue;
            tempindex++;
            SportTypes.push(Sport);
            }
          }
        }
        this.sports = SportTypes;


      });
    }
  }

  


  async onSubmit() {
    this.FormMissingInformation = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const userLatitude = this.form.get('Latitude').value;
        const userLongitude =this.form.get('Longitude').value;
        //const userSport = this.form.get('sportChoice').valueChanges;
        const userDistance = this.form.get('Distance').value;
        await this.fiterService
          .getAllitemsDistanceSport(this.userEmail,userLatitude,userLongitude,userDistance,this.selectedSport)
          .subscribe((items: any) => {
            console.log(items);
            this.itemsGet = items;
            for (var itemPush of this.itemsGet) {
              
              let marker: any = new google.maps.Marker;
              marker.markerOptions = { draggable: false,clickable: true};
              marker.markerOptions.title = itemPush.itemId.id;
              marker.markerPositions = itemPush.location;
              this.markers.push(marker);
            }     
          });

          await this.fiterService
          .getAllitemsDistanceSport(this.userEmail,userLatitude,userLongitude,userDistance,this.selectedSport)
          .subscribe((items: any) => {
            console.log(items);
            this.itemsGet = items;
            for (var itemPush of this.itemsGet) {
              
              let marker: any = new google.maps.Marker;
              marker.markerOptions = { draggable: false,clickable: true};
              marker.markerOptions.title = itemPush.itemId.id;
              marker.markerPositions = itemPush.location;
              this.markers.push(marker);
            }     
          });
      } catch (err) {
        this.FormMissingInformation = true;
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }
  

}
interface Sport {
   value: string;
   viewValue: string;
}
interface itemId{
   space:string;
   id:string;
}

interface userId {
  space:string;
  email:string;
}

interface location {
  lat:number;
  lng:number;
}

interface item {
   itemId:itemId;
   name:string;
   type:string;
   createdTimestamp:string;
   active:boolean;
   createdBy:userId;
   location:location;
   itemAttributes:any;
}
