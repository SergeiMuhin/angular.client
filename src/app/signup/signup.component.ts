import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { EventEmitterService } from '../event-emitter.service';
import { DataService } from "../services/data.service"; 

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  form: FormGroup;
  public signupInvalid: boolean = false;
  private formSubmitAttempt: boolean = false;
  private returnUrl: string = '';

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private eventEmitterService: EventEmitterService,
    private data: DataService
  ) {}

  async ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.email],
      username: ['', Validators.required],
      role: ['', Validators.required],
      avatar: ['', Validators.required],
    });
    this.changeMySidebar(true);
    this.firstComponentFunction();
    this.data.logOutUser();
  }

  async onSubmit() {
    this.signupInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const email = this.form.get('email').value;
        const username = this.form.get('username').value;
        const role = this.form.get('role').value;
        const avatar = this.form.get('avatar').value;

        const user = {
          email: email,
          username: username,
          role: role,
          avatar: avatar,
        };

        await this.userService.signup(user).subscribe((user: any) => {
          console.log(user);
          this.router.navigateByUrl('/login');
          this.changeMySidebar(false);
          this.firstComponentFunction();
          //alert('Hi, my name is ' + user.username + " and I'm a " + user.role);
        });
      } catch (err) {
        this.signupInvalid = true;
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }
  firstComponentFunction(){    
    this.eventEmitterService.onFirstComponentButtonClick();    
  } 
  changeMySidebar(show: boolean){
    this.data.changeShowSidebar(show);
  }
}
