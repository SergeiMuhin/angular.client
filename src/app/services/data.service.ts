import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private eMail: string = "";
  private space: string = "";
  private isUserOnLine: boolean = false;

  private messageSource = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  private showMySidebar: boolean = false;

  constructor() { }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }

  public changeShowSidebar(show: boolean){
    this.showMySidebar = show;
  }
  public getShowSidebar(): boolean {
    return this.showMySidebar;
  }

  public loginUser(eMail: string , space: string){
    if(this.isUserOnLine){
      this.eMail = eMail;
      this.space = space;
      this.isUserOnLine = true;
    } else {
      alert('Hi, my name is ' + eMail + " and I'm a logged in");
    }
  }
  public logOutUser(){
    this.eMail = "";
    this.space = "";
    this.isUserOnLine = true;
  }
  public getSpcae(): string{
    return this.space;
  }
  public getEmail(): string{
    return this.eMail;
  }

}
