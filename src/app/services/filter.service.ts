import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  authenticationState = new BehaviorSubject(false);
  constructor(private http: HttpClient) {}
  
  userSpace: string = '2021a.vladislavb';
  baseUrl: string = '/items/'; // Items instead.

  public getAllitemsDistanceSport(userEmail:string,lat:string,lng:string,distance:string,sport:string): any {
    return this.http.post<any>('/operations',{ type: 'getAllFieldsBySportAndDistance', item:{itemID:{space:'',id:''}}, invokedBy:{userId:{space:this.userSpace,email:userEmail}},operationAttributes:{sport:sport,distance:distance,lat:lat,lng:lng} });
  }
  
  public postAddSport(userEmail:string,itemfieldId:string,itemSportId:string): any {
    return this.http.post('/operations',{ type: 'bindSportToField', item:{itemID:{space:this.userSpace,id:itemfieldId}}, invokedBy:{userId:{space:this.userSpace,email:userEmail}},operationAttributes:{sportSpace:this.userSpace,sportId:itemSportId} });
  }

 // /dts/items/{userSpace}/{userEmail}/{itemSpace}/{itemId}
  public getItem(itemId:string): any {
    return this.http.get(this.baseUrl + '' + this.userSpace + '/s2@gmail.com/' + this.userSpace +'/'+ itemId);
  }
 // localhost:8081/dts/items/userSpace/userEmail/search/byType/type_2
  public getSports(): any {
    return this.http.get(this.baseUrl + '' + this.userSpace + '/s2@gmail.com/search/byType/sport');
  }

  public getField(): any {
    return this.http.get(this.baseUrl + '' + this.userSpace + '/s2@gmail.com/search/byType/field');
  }
  ///dts/items/{userSpace}/{userEmail}/{itemSpace}/{itemId}/children
  public getAllfieldChildren(itemId:string,userEmail:string): any{
    return this.http.get(this.baseUrl + '' + this.userSpace + '/'+userEmail+'/' + this.userSpace +'/'+ itemId+'/children');
  }
}

