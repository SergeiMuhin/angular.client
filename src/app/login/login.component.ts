import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Data, Router } from '@angular/router';
import { EventEmitterService } from '../event-emitter.service';
import { DataService } from "../services/data.service"; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  public loginInvalid: boolean = false;
  private formSubmitAttempt: boolean = false;
  private returnUrl: string = '';

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private eventEmitterService: EventEmitterService,
    private data: DataService,
    ){}

  async ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.email],
      space: ['', Validators.required],
    });
    this.changeMySidebar(false);
    this.firstComponentFunction();
  }

  async onSubmit() {
    this.loginInvalid = false;
    this.formSubmitAttempt = false;
    if (this.form.valid) {
      try {
        const userEmail = this.form.get('email').value;
        const userSpace = this.form.get('space').value;

        await this.userService
          .login(userSpace, userEmail)
          .subscribe((user: any) => {
            console.log(user);
            this.data.loginUser(userEmail,userSpace);
            this.router.navigateByUrl('/Filter');
            this.changeMySidebar(true);
            this.firstComponentFunction();
            //alert('Hi, my name is ' + user.username + " and I'm a " + user.role);
          });
      } catch (err) {
        this.loginInvalid = true;
      }
    } else {
      this.formSubmitAttempt = true;
    }
  }
  firstComponentFunction(){    
    this.eventEmitterService.onFirstComponentButtonClick();    
  } 
  changeMySidebar(show: boolean){
    this.data.changeShowSidebar(show);
  }
}
